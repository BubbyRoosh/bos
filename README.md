# bos
Simple crux-compatible package manager.

There is no dependency management *yet*

## Usage

bos works similarly to [kiss](https://github.com/kiss-community/kiss) in which you build a package or set of packages in one command, then install with a separate command

Passing no arguments will display the help dialog

### Config
(Read bos.example.conf)

Important variable is `packagedirs`

Formatted like `PATH`; directories separated by ':'

### Search
Packages can be searched for in two different ways: grepping and exact matches
#### Grep
`bos g <pkg>`

Example: "ash"

`bos g ash`
(Results such as bash or dash will show up depending on what packages you have available)

#### Exact
`bos s <pkg>`

Example: "ash"

`bos s ash`
(Nothing will show up unless you have an ash package)

### Build
`bos b <pkg>`

Example: vim, libpcre, zsh

`bos b vim libpcre zsh`

### Install
`bos i <pkg>`

Example: vim, libpcre, zsh

`bos i vim libpcre zsh`

### Uninstall
`bos r <pkg>`

Example: vim, libpcre, zsh

`bos r vim libpcre zsh`
